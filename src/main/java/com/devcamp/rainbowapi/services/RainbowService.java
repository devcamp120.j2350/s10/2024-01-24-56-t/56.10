package com.devcamp.rainbowapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

@Service
public class RainbowService {
    String[] rainbowList =  {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};

    public ArrayList<String> filterRainbowlist(String filter) {
        ArrayList<String> filteredRainbows = new ArrayList<>();

        for (String rainbow: this.rainbowList) {
            if (rainbow.contains(filter)) {
                filteredRainbows.add(rainbow);
            }
        }

        return filteredRainbows;
    }

    public String getRainbowByIndex(Integer index) {           
        if (index <= -1 || index  > 6) {
            return "";
        }

        return this.rainbowList[index];
    }
}

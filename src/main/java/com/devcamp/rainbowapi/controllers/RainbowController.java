package com.devcamp.rainbowapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rainbowapi.services.RainbowService;

@CrossOrigin
@RestController
public class RainbowController {
    @Autowired
    RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> filterRainbowlist(@RequestParam("keyword") String filter) {
        System.out.println("Filter : " + filter);      

        return this.rainbowService.filterRainbowlist(filter);
    }

    @GetMapping("/rainbow-request-param/{index}")
    public String getRainbowByIndex(@PathVariable Integer index) {
        System.out.println("Index : " + index);      

        return this.rainbowService.getRainbowByIndex(index);
    }
    
}
